package xyz.gzkahjl.activiti.delegate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author: gzkahjl
 * @date: 2020/5/26 04:08
 * @description:
 */
public class HelloBean {

    /** logger */
    private static final Logger LOGGER = LoggerFactory.getLogger(HelloBean.class);

    public void sayHello() {
        System.out.println("hello activiti.");
    }

}
