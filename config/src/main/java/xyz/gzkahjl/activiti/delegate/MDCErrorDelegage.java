package xyz.gzkahjl.activiti.delegate;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author: gzkahjl
 * @date: 2020/5/24 17:38
 * @description:
 */
public class MDCErrorDelegage implements JavaDelegate {


    /** logger */
    private static final Logger LOGGER = LoggerFactory.getLogger(MDCErrorDelegage.class);

    public void execute(DelegateExecution execution) {
        LOGGER.info("run MDCErrorDelegage");
        throw new RuntimeException("only test");
    }

}
