package xyz.gzkahjl.activiti.config;

import org.activiti.engine.ProcessEngineConfiguration;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author gzk
 * @date 2020/5/24 21:34
 * 创建流程引擎配置测试
 */

public class ConfigTest {

    /** logger */
    private static final Logger log = LoggerFactory.getLogger(ConfigTest.class);
    /**
     * 基于 资源文件去加载
     */
    @Test
    public void testConfig1() {
        ProcessEngineConfiguration configuration = ProcessEngineConfiguration
                .createProcessEngineConfigurationFromResourceDefault();
        log.info("configuration = {}", configuration);
    }

    /**
     * 直接new一个 StandaloneProcessEngineConfiguration 出来
     */
    @Test
    public void testConfig2() {
        ProcessEngineConfiguration configuration = ProcessEngineConfiguration
                .createStandaloneProcessEngineConfiguration();
        log.info("configuration = {}", configuration);
    }
}
