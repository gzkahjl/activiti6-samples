package xyz.gzkahjl.activiti.config;

import org.activiti.engine.logging.LogMDC;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.test.ActivitiRule;
import org.activiti.engine.test.Deployment;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author: gzkahjl
 * @date: 2020/5/24 17:21
 * @description:activitimdc配置测试
 */
public class ConfigMDCTest {
    /**
     * 自动创建流程引擎并且配置
     */
    @Rule
    public ActivitiRule activitiRule = new ActivitiRule("activiti_mdc.cfg.xml");

    //    public ActivitiRule activitiRule = new ActivitiRule();
    @Test
    //在启动单元测试以前把my-process.bpmn20.xml部署到流程1引擎中
//    @Deployment(resources = {"my-process.bpmn20.xml"})
    @Deployment(resources = {"xyz/gzkahjl/activiti/my-process_mdcerror.bpmn20.xml"})
    public void test() {
        //打开mdc
        LogMDC.setMDCEnabled(true);
        ProcessInstance processInstance = activitiRule.getRuntimeService().startProcessInstanceByKey("my-process");
        assertNotNull(processInstance);
        Task task = activitiRule.getTaskService().createTaskQuery().singleResult();

        assertEquals("Activiti is awesome!", task.getName());

    }

}
